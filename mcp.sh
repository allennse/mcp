#!/bin/bash

#TODO
# getopts to supress individual services
# health check
# log viewer
# scan logs for errors
# refactor service definition into configs

CURDIR=`pwd`
SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCEDIR="$HOME/Source/microservices"
REGISTRY="$SOURCEDIR/jhipster-registry"
GATEWAY="$SOURCEDIR/gateway"
UAASE="$SOURCEDIR/uaase"
BILLING="$SOURCEDIR/billing"
LEGACY="$SOURCEDIR/legacy-service"
LIBRARYDIR="$SOURCEDIR/library-manager"
LIBRARY="$LIBRARYDIR/backend"
SITEDIR="$SOURCEDIR/site-manager"
SITE="$SITEDIR/backend"
LOGS=$SOURCEDIR
PIDS=(registryPID gatewayPID uaasePID billingPID legacyPID libraryPID sitePID)
FRONTENDS=(libraryFrontendPID siteFrontendPID)
echo $LOGS

startMicroservice(){
	service=$(echo $1 | tr '[:lower:]' '[:upper:]' )
	if [[ ! -d ${!service} ]]; then
		echo "No such service: "$service
		return
	fi
	cd ${!service}
	pid=$1PID
	log=$LOGS/$1.log
	mvn spring-boot:run > $log &
	eval $pid=$!
	printf "Starting %8s %6d log: %s\n" $1 ${!pid} $log
	cd $CURDIR
}

startFrontend(){
	service=$(echo $1 | tr '[:lower:]' '[:upper:]' )DIR
	if [[ ! -d ${!service} ]]; then
		echo "No such frontend: "$service
		return
	fi
	cd ${!service}/frontend
	pid=$1FrontendPID
	log=$LOGS/$1-frontend.log
	npm run serve:local > $log &
	eval $pid=$!
	printf "Starting %8s frontend %6d log: %s\n" $1 ${!pid} $log
	cd $CURDIR
}

stopMicroservice(){
	if [[ "$1" == "-f" ]]; then
		service=$(echo $2 | tr '[:lower:]' '[:upper:]' )
		pid=$2FrontendPID
	else
		service=$(echo $1 | tr '[:lower:]' '[:upper:]' )
		pid=$1PID
	fi
	if [[ ! -d ${!service} ]]; then
		echo "No such service: "$service
		return
	fi
	kill ${!pid}
	echo "Stopping "$pid ${!pid}
	unset $pid
}

restartMicroservice(){
	echo "Restarting "$1
	stopMicroservice $1
	startMicroservice $1
}

cleanup(){
	for pid in ${PIDS[@]}; do
		if [[ -n ${!pid} ]]; then
			echo $pid ${!pid}
			kill ${!pid}
		fi
	done
	for pid in ${FRONTENDS[@]}; do
		if [[ -n ${!pid} ]]; then
			echo $pid ${!pid}
			kill ${!pid}
		fi
	done
	echo -e '\nall microservices killed\nEnd of Line'
	exit 0
}

trap cleanup SIGINT

while getopts ":" opt; do
	case "$opt" in
		*)
			;;
	esac
done

echo 'starting all microservices'
cd $LOGS

startMicroservice registry
(tail -f -n0 $LOGS/registry.log) | grep -q 'Application '"'jhipster-registry'"' is running!' 

startMicroservice gateway
startMicroservice uaase
startMicroservice billing
startMicroservice legacy
startMicroservice library
startMicroservice site

echo 'all microservices started'
(tail -f -n +1 $LOGS/gateway.log) | grep -q 'Application '"'gateway'"' is running!' 
(tail -f -n +1 $LOGS/uaase.log) | grep -q 'Application '"'uaase'"' is running!' 
(tail -f -n +1 $LOGS/library.log) | grep -q 'Application '"'library-manager'"' is running!' 
(tail -f -n +1 $LOGS/site.log) | grep -q 'Application '"'site-manager'"' is running!' 
echo 'all microservices running'
osascript -e 'display alert "MCP" message "All microservices running"' > /dev/null

while true; do
	read -p "> " cmd
	terms=($cmd)
	op=${terms[0]}
	unset terms[0]
	case "$op" in
		"start")
			if [[ "${terms[1]}" == "-f" ]]; then
				startFrontend ${terms[2]}
			else
				startMicroservice ${terms[*]}
			fi
			;;
		"stop")
			stopMicroservice ${terms[*]}
			;;
		"restart")
			restartMicroservice ${terms[*]}
			;;
		"status")
			#http --headers localhost:9995/management/health | sed -n 1p | awk '{print $2}'
			printf "%18s\t%6s\t%3s\t%3s\n" 'service' 'pid' 'cpu' 'mem'
			for pid in ${PIDS[@]}; do
				if [[ -n ${!pid} ]]; then
					printf "%18s\t%6s\t%3s\t%3s\n" $pid $(ps -o pid=,%cpu=,%mem= -p ${!pid})
				fi
			done
			for pid in ${FRONTENDS[@]}; do
				if [[ -n ${!pid} ]]; then
					printf "%18s\t%6s\t%3s\t%3s\n" $pid $(ps -o pid=,%cpu=,%mem= -p ${!pid})
				fi
			done
			;;
		"errors")
			echo 'show error stacks'
			;;
		*)
			echo "Unknown command: "$cmd
			;;
	esac
done

