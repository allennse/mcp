#!/bin/bash

set -e

devStart () {
    devSetServerStatus $2 Active $1
    gcloud config set project se-dev-235715
    gcloud compute instances start dev-win-lucee-$1 --async
}
 
devStop () {
    if (( $# != 2 ))
    then
        echo "Usage: devStop VM# 'dbPass', e.g. devStop 1 'dbPass'"
        return
    fi
 
    devSetServerStatus $2 Inactive $1
    gcloud config set project se-dev-235715
    gcloud compute instances stop dev-win-lucee-$1 --async
}
 
devSetServerStatus () {
    if (( $# != 3 ))
    then
        echo "Usage: devSetServerStatus 'dbPass' status VM#, e.g. devSetServerStatus 'dbPass' Active 1"
        return
    fi
 
    sqlcmd -S tcp:dev-sql-1.sedev.local,1433 -U sa -P $1 -Q "USE SEMerchants;UPDATE servers SET status = '$2' where vchservername = 'dev-win-lucee-$3';"
}

getPassword() {
#	lpass login accounts@allenng.com
#	pw=lpass show --password Work/db
#	lpass logout

	security find-generic-password -D 'cli password' -l 'vm db' -w
}

if (( $# != 2 ));then
	echo "Usage: dev.sh [-u vm# | -d vm#]"
	exit 1
fi

while getopts ":u:d:" opt; do
	case "$opt" in
		u)
			devStart $OPTARG `getPassword`
			;;
		d)
			devStop $OPTARG `getPassword`
			;;
		*)
			echo 'Well, that was unexpected'
			;;
	esac
done
